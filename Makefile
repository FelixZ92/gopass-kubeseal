GIT_COMMIT  ?= $(shell git rev-parse HEAD)
VERSION ?= $(shell git describe)
BUILD_TIME ?= $(shell date +'%Y-%m-%d_%T')

PROJECT_NAME := "gopass-kubeseal"
PKG := "gitlab.com/felixz92/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

# Get source files, ignore vendor directory
SRC_FILES := $(shell find . -type f -name '*.go' -not -path "./vendor/*")

.DEFAULT_GOAL := help

.PHONY: all help tidy build unit-test gofmt golint clean dep

help:
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

all: build

dep:
	go mod download

tidy: ## Clean up Go modules by adding missing and removing unused modules
	go mod tidy

build: dep ## Compile the operator
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/gopass-kubeseal \
	  -ldflags "-X gitlab.com/felixz92/gopass-kubeseal/pkg/version.Sha1ver=$(GIT_COMMIT) -X gitlab.com/felixz92/gopass-kubeseal/pkg/version.Version=$(VERSION) -X gitlab.com/felixz92/gopass-kubeseal/pkg/version.BuildTime=$(BUILD_TIME)"
#gitlab.com/felixz92/gopass-kubeseal/pkg/version.BuildTime
unit-test: dep ## Run unit tests
	go test -v ./... -cover

gofmt: ## Format the Go code with `gofmt`
	@gofmt -s -l -w $(SRC_FILES)

golint: dep ## Run linter on operator code
	for file in $(SRC_FILES); do \
		golint $${file}; \
		if [ -n "$$(golint $${file})" ]; then \
			exit 1; \
		fi; \
	done

clean: ## Clean binary artifacts
	rm -rf build/_output
	rm -rf dist/
