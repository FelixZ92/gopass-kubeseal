# gopass-kubeseal

Read secret from passwordstore, encrypt them with sealed-secrets and directly deploy them to k8s (or dump to file)

```
Usage:
  gopass-kubeseal [command]

Available Commands:
  apply       Applies secrets stored in password as a sealed secret in k8s
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  seal        seal secrets stored in password and dump them as a file
  version     Displays version information


Flags:
      --as string                      Username to impersonate for the operation
      --as-group stringArray           Group to impersonate for the operation, this flag can be repeated to specify multiple groups.
      --as-uid string                  UID to impersonate for the operation
      --certificate-authority string   Path to a cert file for the certificate authority
      --client-certificate string      Path to a client certificate file for TLS
      --client-key string              Path to a client key file for TLS
      --cluster string                 The name of the kubeconfig cluster to use
      --context string                 The name of the kubeconfig context to use
      --controller-name string         Name of sealed-secrets controller. (default "sealed-secrets-controller") (default "sealed-secrets-controller")
      --controller-namespace string    Namespace of sealed-secrets controller. (default "kube-system") (default "kube-system")
      --dry-run                        Do not deploy to kubernetes, but print the generated sealed secret to stdout
  -h, --help                           help for gopass-kubeseal
      --insecure-skip-tls-verify       If true, the server's certificate will not be checked for validity. This will make your HTTPS connections insecure
      --kubeconfig string              Path to a kube config. Only required if out-of-cluster
  -n, --namespace string               If present, the namespace scope for this CLI request
      --password string                Password for basic authentication to the API server
      --password-store-dir string      Path to the password store. Environment var PASSWORD_STORE_DIR takes precedence (default ~/.password-store) (default "~/.password-store")
      --request-timeout string         The length of time to wait before giving up on a single server request. Non-zero values should contain a corresponding time unit (e.g. 1s, 2m, 3h). A value of zero means don't timeout requests. (default "0")
      --server string                  The address and port of the Kubernetes API server
      --tls-server-name string         If provided, this name will be used to validate server certificate. If this is not provided, hostname used to contact the server is used.
      --token string                   Bearer token for authentication to the API server
      --user string                    The name of the kubeconfig user to use
      --username string                Username for basic authentication to the API server
```

## Applying by bulk
```
$ cat test.yaml
secrets:
  - path: foo/bar/baz
    targets:
      - namespace: monitoring
        outputDirectory: out
    secretName: baz
  - path: foo/bar/boz
    targets:
      - namespace: kube-system
        outputDirectory: out
    secretName: boz

$ gopass-kubeseal apply bulk -f test.yaml
write baz to out/baz.yaml
sealedsecrets.bitnami.com "baz" created
write boz to out/boz.yaml
sealedsecrets.bitnami.com "boz" created

```
