package util

import (
	"fmt"
	"io"
	"k8s.io/apimachinery/pkg/runtime"
	runtimeserializer "k8s.io/apimachinery/pkg/runtime/serializer"
)

func Serialize(out io.Writer, codecs runtimeserializer.CodecFactory, gv runtime.GroupVersioner, obj runtime.Object) error {
	info, _ := runtime.SerializerInfoForMediaType(codecs.SupportedMediaTypes(), runtime.ContentTypeYAML)
	prettyEncoder := info.PrettySerializer
	if prettyEncoder == nil {
		prettyEncoder = info.Serializer
	}

	prettyEnc := codecs.EncoderForVersion(prettyEncoder, gv)

	buf, err := runtime.Encode(prettyEnc, obj)

	if err != nil {
		return err
	}

	_, err = out.Write(buf)
	if err != nil {
		return err
	}

	_, err = fmt.Fprint(out, "\n")
	if err != nil {
		return err
	}

	return nil
}
