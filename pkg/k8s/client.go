package k8s

import (
	"context"
	"fmt"
	ssv1alpha1 "github.com/bitnami-labs/sealed-secrets/pkg/apis/sealed-secrets/v1alpha1"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/kubeseal"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

// InitClient initializes the k8s client
func InitClient(config clientcmd.ClientConfig) (*rest.RESTClient, error) {
	err := kubeseal.AddToScheme(scheme.Scheme)
	if err != nil {
		return nil, err
	}

	conf, err := config.ClientConfig()
	if err != nil {
		return nil, err
	}

	crdConfig := *conf
	crdConfig.ContentConfig.GroupVersion = &schema.GroupVersion{Group: kubeseal.GroupName, Version: kubeseal.GroupVersion}
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = serializer.NewCodecFactory(scheme.Scheme)
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()
	restClient, err := rest.UnversionedRESTClientFor(&crdConfig)
	if err != nil {
		return nil, err
	}
	return restClient, nil
}

func create(ctx context.Context, sealedSecret *ssv1alpha1.SealedSecret, k8sClient *rest.RESTClient) error {
	err := k8sClient.Post().
		Namespace(sealedSecret.Namespace).
		Name(sealedSecret.Name).
		Resource("sealedsecrets").
		Body(sealedSecret).
		Do(ctx).
		Error()
	if err != nil {
		return err
	}

	return nil
}

// CreateOrUpdate creates or updates a SealedSecret
func CreateOrUpdate(ctx context.Context, sealedSecret *ssv1alpha1.SealedSecret, k8sClient *rest.RESTClient) error {
	currentSealed, err := fetch(ctx, sealedSecret, k8sClient)
	if errors.IsNotFound(err) {
		err = create(ctx, sealedSecret, k8sClient)
		if err != nil {
			return err
		}

		fmt.Printf("sealedsecrets.bitnami.com \"%s\" in namespace \"%s\" created\n", sealedSecret.Name, sealedSecret.Namespace)

		return nil
	}

	if err != nil {
		return err
	}

	sealedSecret.ResourceVersion = currentSealed.ResourceVersion

	err = update(ctx, sealedSecret, k8sClient)

	if err != nil {
		fmt.Println(err)
		return err
	}

	fmt.Printf("sealedsecrets.bitnami.com \"%s\" in namespace \"%s\" configured\n", sealedSecret.Name, sealedSecret.Namespace)

	return nil
}

func update(ctx context.Context, sealedSecret *ssv1alpha1.SealedSecret, k8sClient *rest.RESTClient) error {
	err := k8sClient.Put().
		Namespace(sealedSecret.Namespace).
		Name(sealedSecret.Name).
		Resource("sealedsecrets").
		Body(sealedSecret).
		Do(ctx).
		Error()
	if err != nil {
		return err
	}

	return nil
}

func fetch(ctx context.Context, sealedSecret *ssv1alpha1.SealedSecret, k8sClient *rest.RESTClient) (ssv1alpha1.SealedSecret, error) {
	currentSealed := ssv1alpha1.SealedSecret{}
	err := k8sClient.Get().
		Namespace(sealedSecret.Namespace).
		Name(sealedSecret.Name).
		Resource("sealedsecrets").
		Do(ctx).Into(&currentSealed)
	if err != nil {
		return ssv1alpha1.SealedSecret{}, err
	}

	return currentSealed, nil
}
