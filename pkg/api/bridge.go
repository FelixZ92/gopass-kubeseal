package api

import (
	"context"
	"crypto/rsa"
	"fmt"
	ssv1alpha1 "github.com/bitnami-labs/sealed-secrets/pkg/apis/sealed-secrets/v1alpha1"
	"github.com/gopasspw/gopass/pkg/gopass/api"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/gopass"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/k8s"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/kubeseal"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/util"
	"io"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"strings"
)

func NewGopassKubesealBridge(ctx context.Context, config clientcmd.ClientConfig, ksCfg kubeseal.SealedSecretsConfig) (*GopassKubesealBridge, error) {
	client, err := k8s.InitClient(config)
	if err != nil {
		return nil, err
	}

	gp, err := api.New(ctx)
	if err != nil {
		return nil, err
	}

	pk, err := kubeseal.FetchPubKey(ksCfg, config)
	if err != nil {
		return nil, err
	}

	return &GopassKubesealBridge{
		k8sClient: client,
		gopassAPI: gp,
		pubkey:    pk,
		ctx:       ctx,
	}, nil
}

type GopassKubesealBridge struct {
	gopassAPI *api.Gopass
	pubkey    *rsa.PublicKey
	k8sClient *rest.RESTClient
	ctx       context.Context
}

func (s *Secret) writeSecret(secret *ssv1alpha1.SealedSecret, dryRun bool) error {
	var f io.Writer
	target := s.findTarget(secret.Namespace)

	if dryRun || target.OutputDirectory == "" {
		f = os.Stdout
	} else {
		err := os.MkdirAll(target.OutputDirectory, 0777)
		if err != nil {
			return err
		}

		filename := fmt.Sprintf("%s/%s.yaml", target.OutputDirectory, secret.Name)
		f, err = os.Create(filename)
		if err != nil {
			return err
		}
		fmt.Printf("write %s to %s\n", secret.Name, filename)
	}

	err := util.Serialize(f, scheme.Codecs, ssv1alpha1.SchemeGroupVersion, secret)
	if err != nil {
		return err
	}

	return nil
}

func (b *GopassKubesealBridge) WriteSecrets(item *Secret, dryRun bool, fallbackNamespace string) error {
	sealed, err := item.CreateSealedSecrets(b.ctx, b.gopassAPI, b.pubkey, fallbackNamespace)
	if err != nil {
		return err
	}

	for _, secret := range sealed {
		if err = item.writeSecret(secret, dryRun); err != nil {
			return err
		}
	}

	return nil
}

func (b *GopassKubesealBridge) ApplySecrets(item *Secret, dryRun, disableOutput bool, fallbackNamespace string) error {
	sealed, err := item.CreateSealedSecrets(b.ctx, b.gopassAPI, b.pubkey, fallbackNamespace)
	if err != nil {
		return err
	}

	for _, secret := range sealed {
		if !disableOutput {
			if err = item.writeSecret(secret, dryRun); err != nil {
				return err
			}
		}

		if !dryRun {
			if err = k8s.CreateOrUpdate(b.ctx, secret, b.k8sClient); err != nil {
				return err
			}
		}
	}

	return nil
}

type SecretMeta struct {
	Annotations map[string]string `json:"annotations,omitempty"`
	Labels      map[string]string `json:"labels,omitempty"`
}

type Target struct {
	Namespace       string `yaml:"namespace,omitempty"`
	OutputDirectory string `yaml:"outputDirectory,omitempty"`
}

// Secret represents a reference to a gopass entry with namespace & name
type Secret struct {
	Path       string            `yaml:"path"`
	Targets    []Target          `yaml:"targets,omitempty"`
	SecretName string            `yaml:"secretName,omitempty"`
	SecretSpec gopass.SecretSpec `yaml:"spec,omitempty"`
	Metadata   SecretMeta        `yaml:"metadata,omitempty"`
}

// SecretList represents a list of Secret
type SecretList struct {
	Items []Secret `yaml:"secrets"`
}

// CreateKubernetesSecrets turns a Secret to a k8s-secret object
func (s *Secret) CreateKubernetesSecrets(ctx context.Context, gp *api.Gopass) ([]*v1.Secret, error) {
	pwsSecret, err := gopass.NewPasswordStoreSecret(ctx, gp, s.Path, s.SecretSpec)
	if err != nil {
		return nil, err
	}
	var k8sSecrets []*v1.Secret
	for _, t := range s.Targets {
		secret := pwsSecret.ToKubernetesSecret(t.Namespace, s.SecretName, s.Metadata.Labels, s.Metadata.Annotations)
		k8sSecrets = append(k8sSecrets, secret)
	}
	return k8sSecrets, nil
}

// CreateSealedSecrets turns a Secret to a SealedSecret
func (s *Secret) CreateSealedSecrets(ctx context.Context, gp *api.Gopass, pubkey *rsa.PublicKey, configNamespace string) ([]*ssv1alpha1.SealedSecret, error) {
	k8sSecrets, err := s.CreateKubernetesSecrets(ctx, gp)
	if err != nil {
		return nil, err
	}

	var sealedSecrets []*ssv1alpha1.SealedSecret
	for _, secret := range k8sSecrets {
		sealed, err := kubeseal.Seal(secret, scheme.Codecs, pubkey, configNamespace)
		if err != nil {
			return sealedSecrets, err
		}
		sealedSecrets = append(sealedSecrets, sealed)
	}
	return sealedSecrets, nil
}

func (s *Secret) findTarget(namespace string) Target {
	for _, target := range s.Targets {
		if target.Namespace == namespace {
			return target
		}
	}

	return Target{}
}

// SecretBuilder creates a Secret
func SecretBuilder(path string, targetNamespaces []string, secretName string, fallbackNamespace string) *Secret {
	if len(targetNamespaces) == 0 {
		targetNamespaces = []string{fallbackNamespace}
	}

	var targets []Target
	if len(targetNamespaces) > 0 {
		for _, namespace := range targetNamespaces {
			targets = append(targets, Target{
				Namespace:       namespace,
				OutputDirectory: "",
			})
		}
	}

	var name string
	if secretName != "" {
		name = secretName
	} else {
		name = strings.ReplaceAll(path, "/", "-")
	}

	secret := &Secret{
		Path:       path,
		Targets:    targets,
		SecretName: name,
	}
	return secret
}
