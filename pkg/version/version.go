package version

var (
	Version   string // version of the programs
	Sha1ver   string // sha1 revision used to build the program
	BuildTime string // when the executable was built
)
