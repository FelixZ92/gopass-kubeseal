package kubeseal

import (
	ssv1alpha1 "github.com/bitnami-labs/sealed-secrets/pkg/apis/sealed-secrets/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

// GroupName represents the api group
const GroupName = "bitnami.com"

// GroupVersion represents the api group version
const GroupVersion = "v1alpha1"

var schemeGroupVersion = schema.GroupVersion{Group: GroupName, Version: GroupVersion}

var (
	schemeBuilder = runtime.NewSchemeBuilder(addKnownTypes)
	// AddToScheme adds sealed secret crd to k8s client
	AddToScheme = schemeBuilder.AddToScheme
)

func addKnownTypes(scheme *runtime.Scheme) error {
	scheme.AddKnownTypes(schemeGroupVersion,
		&ssv1alpha1.SealedSecret{},
	)
	metav1.AddToGroupVersion(scheme, schemeGroupVersion)
	return nil
}
