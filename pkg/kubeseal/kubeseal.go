package kubeseal

import (
	"context"
	"crypto/rsa"
	"errors"
	"fmt"
	ssv1alpha1 "github.com/bitnami-labs/sealed-secrets/pkg/apis/sealed-secrets/v1alpha1"
	"io"
	"io/ioutil"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	runtimeserializer "k8s.io/apimachinery/pkg/runtime/serializer"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/cert"
)

// SealedSecretsConfig is the config for kubeseal
type SealedSecretsConfig struct {
	ControllerNamespace string
	ControllerName      string
	ControllerPort      string
}

// Seal reads a k8s Secret resource parsed from an input reader by a given codec, encrypts all its secrets
// with a given public key, using the name and namespace found in the input secret, unless explicitly overridden
// by the overrideName and overrideNamespace arguments.
func Seal(secret *v1.Secret, codecs runtimeserializer.CodecFactory, pubKey *rsa.PublicKey, configNamespace string) (*ssv1alpha1.SealedSecret, error) {

	if secret.GetName() == "" {
		return nil, fmt.Errorf("missing metadata.name in input Secret")
	}

	if ssv1alpha1.SecretScope(secret) != ssv1alpha1.ClusterWideScope && secret.GetNamespace() == "" {
		secret.SetNamespace(configNamespace)
	}

	// Strip read-only server-side ObjectMeta (if present)
	secret.SetSelfLink("")
	secret.SetUID("")
	secret.SetResourceVersion("")
	secret.Generation = 0
	secret.SetCreationTimestamp(metav1.Time{})
	secret.SetDeletionTimestamp(nil)
	secret.DeletionGracePeriodSeconds = nil

	ssecret, err := ssv1alpha1.NewSealedSecret(codecs, pubKey, secret)
	if err != nil {
		return nil, err
	}
	return ssecret, nil
}

// FetchPubKey returns the sealed-secret pubkey via kubernetes-proxy
func FetchPubKey(config SealedSecretsConfig, clientConfig clientcmd.ClientConfig) (*rsa.PublicKey, error) {
	certFile, err := openCert(config, clientConfig)
	if err != nil {
		return nil, err
	}
	defer certFile.Close()
	pubKey, err := parseKey(certFile)
	if err != nil {
		return nil, err
	}
	return pubKey, nil
}

func openCert(config SealedSecretsConfig, clientConfig clientcmd.ClientConfig) (io.ReadCloser, error) {
	conf, err := clientConfig.ClientConfig()
	if err != nil {
		return nil, err
	}
	conf.AcceptContentTypes = "application/x-pem-file, */*"
	restClient, err := corev1.NewForConfig(conf)
	if err != nil {
		return nil, err
	}
	return openCertCluster(restClient, config.ControllerNamespace, config.ControllerName, config.ControllerPort)
}

// openCertCluster fetches a certificate by performing an HTTP request to the controller
// through the k8s API proxy.
func openCertCluster(c corev1.CoreV1Interface, namespace, name, port string) (io.ReadCloser, error) {
	f, err := c.
		Services(namespace).
		ProxyGet("http", name, port, "/v1/cert.pem", nil).
		Stream(context.Background())
	if err != nil {
		return nil, fmt.Errorf("cannot fetch certificate: %v", err)
	}
	return f, nil
}

func parseKey(r io.Reader) (*rsa.PublicKey, error) {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return nil, err
	}

	certs, err := cert.ParseCertsPEM(data)
	if err != nil {
		return nil, err
	}

	// ParseCertsPem returns error if len(certs) == 0, but best to be sure...
	if len(certs) == 0 {
		return nil, errors.New("failed to read any certificates")
	}

	firstCert, ok := certs[0].PublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("expected RSA public key but found %v", certs[0].PublicKey)
	}

	return firstCert, nil
}
