package gopass

import (
	bytes2 "bytes"
	"context"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"github.com/google/uuid"
	"github.com/gopasspw/gopass/pkg/ctxutil"
	"github.com/gopasspw/gopass/pkg/gopass/api"
	"github.com/gopasspw/gopass/pkg/gopass/secrets"
	"github.com/gopasspw/gopass/pkg/pwgen"
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"os"
	"strings"
)

const (
	hexType     = "hex"
	defaultType = "default"
	uuidType    = "uuid"
)

// SecretType represents the type of the Secret to be generated (hexType/defaultType)
type SecretType string

func (st SecretType) toString() string {
	return strings.ToLower(string(st))
}

func (sm *secretMeta) generateSecret() (string, error) {
	switch sm.Type.toString() {
	case hexType:
		return randomHexString(int(sm.Size))
	case defaultType:
		return pwgen.GeneratePassword(int(sm.Size), false), nil
	case uuidType:
		return uuid.New().String(), nil
	}
	return "", nil
}

func randomHexString(length int) (string, error) {
	bytes := make([]byte, length/2) // since we're encoding in hex
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

type SecretData struct {
	SecretKey      string            `yaml:"secretKey"`
	AdditionalData map[string]string `yaml:"additionalData,omitempty"`
}

type secretMeta struct {
	Type SecretType `yaml:"type"`
	Size int32      `yaml:"size"`
}

// SecretSpec represents the data to generate secrets
type SecretSpec struct {
	Create bool       `yaml:"create"`
	Meta   secretMeta `yaml:"meta"`
	Data   SecretData `yaml:"data,omitempty"`
}

// PasswordStoreSecret represents the path to secret in the passwords store and it's contents
type PasswordStoreSecret struct {
	Path           string
	Secret         string
	SecretKey      string
	AdditionalData map[string]string
}

type secretBody struct {
	SecretKey      string            `yaml:"secretKey"`
	AdditionalData map[string]string `yaml:"additionalData,omitempty"`
}

func createNewSecret(ctx context.Context, gp *api.Gopass, path string, secretSpec SecretSpec) error {
	password, err := secretSpec.Meta.generateSecret()
	if err != nil {
		return err
	}

	//_, err = newSecret.Add()WriteString("\n---\n")
	//newSecret.Set("secretKey", secretSpec.Data.SecretKey)
	buf := bytes2.NewBufferString("---\n")
	enc := yaml.NewEncoder(buf)
	err = enc.Encode(secretSpec.Data)
	if err != nil {
		return err
	}
	err = enc.Close()
	if err != nil {
		return err
	}

	newSecret, err := secrets.ParseYAML(buf.Bytes())
	if err != nil {
		return err
	}

	newSecret.SetPassword(password)

	err = gp.Set(ctx, path, newSecret)
	_, _ = fmt.Fprintf(os.Stderr, "passwordstore entry %s created\n", path)
	if err != nil {
		return err
	}

	return nil
}

// NewPasswordStoreSecret creates a PasswordStoreSecret from the given path
func NewPasswordStoreSecret(ctx context.Context, gp *api.Gopass, path string, secretSpec SecretSpec) (*PasswordStoreSecret, error) {
	s, err := gp.Get(ctxutil.WithShowParsing(ctx, false), path, "")
	if err != nil {
		if err.Error() == errors.Errorf("entry is not in the password store").Error() && secretSpec.Create {
			err := createNewSecret(ctx, gp, path, secretSpec)
			if err != nil {
				return nil, err
			}
			s, err = gp.Get(ctx, path, "")
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	}

	var secretBody secretBody
	err = yaml.Unmarshal([]byte(s.Body()), &secretBody)
	if err != nil {
		return nil, err
	}

	pw := s.Password()
	if pw == "---" {
		pw = ""
	}

	pss := &PasswordStoreSecret{
		Path:           path,
		Secret:         pw,
		SecretKey:      secretBody.SecretKey,
		AdditionalData: secretBody.AdditionalData,
	}

	return pss, nil
}

// ToKubernetesSecret returns a k8s-secret from the given PasswordStoreSecret
func (pss *PasswordStoreSecret) ToKubernetesSecret(namespace string, name string, labels, annotations map[string]string) *v1.Secret {
	data := map[string][]byte{}
	for key, elem := range pss.AdditionalData {
		data[key] = []byte(elem)
	}
	if pss.SecretKey != "" {
		data[pss.SecretKey] = []byte(pss.Secret)
	}

	s := &v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:        name,
			Namespace:   namespace,
			Annotations: annotations,
			Labels:      labels,
		},
		Data: data,
		Type: v1.SecretTypeOpaque,
	}
	return s
}
