// Package cmd contains commands
/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"k8s.io/client-go/tools/clientcmd"
	"os"
)

var (
	config                    clientcmd.ClientConfig
	namespaceFromClientConfig = func() (string, bool, error) { return config.Namespace() }

	controllerNamespace string
	controllerName      string
	controllerPort      string
	passwordStoreDir    string
	dryRun              bool
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gopass-kubeseal",
	Short: "Read secrets from passwordstore, encrypt them with sealed-secrets and directly deploy them to k8s (or dump to stdout)",
	Long: `Read secret and deploy to k8s: 
gopass-kubeseal apply --path clusters/dev/test-secret --target-namespace test --secret-name test

`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringVar(&controllerName, "controller-name", "sealed-secrets-controller", "Name of sealed-secrets controller. (default \"sealed-secrets-controller\")")
	rootCmd.PersistentFlags().StringVar(&controllerNamespace, "controller-namespace", "kube-system", "Namespace of sealed-secrets controller. (default \"kube-system\")")
	rootCmd.PersistentFlags().StringVar(&controllerPort, "controller-port", "8080", "port of sealed-secrets controller service. (default \"8080\")")
	rootCmd.PersistentFlags().StringVar(&passwordStoreDir, "password-store-dir", "~/.password-store", "Path to the password store. Environment var PASSWORD_STORE_DIR takes precedence (default ~/.password-store)")
	rootCmd.PersistentFlags().BoolVar(&dryRun, "dry-run", false, "Do not deploy to kubernetes, but print the generated sealed secret to stdout")

	config = AddKubectlFlagsToCmd(rootCmd)
}

// AddKubectlFlagsToCmd adds kubectl like flags to a command and returns the ClientConfig interface
// for retrieving the values.
func AddKubectlFlagsToCmd(cmd *cobra.Command) clientcmd.ClientConfig {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	loadingRules.DefaultClientConfig = &clientcmd.DefaultClientConfig
	overrides := clientcmd.ConfigOverrides{}
	kflags := clientcmd.RecommendedConfigOverrideFlags("")
	cmd.PersistentFlags().StringVar(&loadingRules.ExplicitPath, "kubeconfig", "", "Path to a kube config. Only required if out-of-cluster")
	clientcmd.BindOverrideFlags(&overrides, cmd.PersistentFlags(), kflags)
	return clientcmd.NewInteractiveDeferredLoadingClientConfig(loadingRules, &overrides, os.Stdin)
}
