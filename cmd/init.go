package cmd

import (
	"context"
	"os"
)

func initResources(ctx context.Context) error {
	if err := initPasswordStoreEnvironment(); err != nil {
		return err
	}

	return nil
}

func initPasswordStoreEnvironment() error {
	passwordStoreDirEnv := os.Getenv("PASSWORD_STORE_DIR")
	if passwordStoreDirEnv != "" {
		passwordStoreDir = passwordStoreDirEnv
	} else {
		err := os.Setenv("PASSWORD_STORE_DIR", passwordStoreDir)
		if err != nil {
			return err
		}
	}
	return nil
}
