// Package cmd contains commands
/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/api"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/kubeseal"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"os"
)

var sealBulkFile string

// sealBulkCmd represents the applyBulk command
var sealBulkCmd = &cobra.Command{
	Use:           "bulk",
	Short:         "Apply a bunch of secret defined in a yaml file",
	SilenceUsage:  true,
	SilenceErrors: true,
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if sealBulkFile == "" {
			return errors.New("file not set. aborting")
		}
		err := initPasswordStoreEnvironment()
		if err != nil {
			return err
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {

		secretList, err := parseInputFile(sealBulkFile)

		if err != nil {
			return err
		}

		gkb, err := api.NewGopassKubesealBridge(cmd.Context(), config, kubeseal.SealedSecretsConfig{
			ControllerNamespace: controllerNamespace,
			ControllerName:      controllerName,
			ControllerPort:      controllerPort,
		})

		if err != nil {
			return err
		}

		ns, _, _ := namespaceFromClientConfig()

		var hasError bool
		for _, item := range secretList.Items {
			err = gkb.WriteSecrets(&item, dryRun, ns)
			if err != nil {
				hasError = true
				fmt.Printf("error on item %s: %s\n", item.Path, err)
				panic(err)
			}
		}

		if hasError {
			return err
		}

		return nil
	},
}

func init() {
	sealBulkCmd.Flags().StringVarP(&sealBulkFile, "file", "f", "", "File to parse and apply. Required")
	sealCmd.AddCommand(sealBulkCmd)
}

func parseInputFile(file string) (*api.SecretList, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	input := f

	b, err := ioutil.ReadAll(input)
	if err != nil {
		return nil, err
	}

	var secretList api.SecretList
	err = yaml.Unmarshal(b, &secretList)

	if err != nil {
		return nil, err
	}
	return &secretList, nil
}
