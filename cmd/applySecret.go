package cmd

import (
	"errors"
	"github.com/spf13/cobra"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/api"
	"gitlab.com/felixz92/gopass-kubeseal/pkg/kubeseal"
)

var (
	applySecretTargetNamespaces []string
	applySecretSecretName       string
	applySecretPath             string
)

// applyCmd represents the apply command
var applySecretCmd = &cobra.Command{
	Use:   "secret",
	Short: "Applies a secret stored in password as a sealed secret in k8s",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if applySecretPath == "" {
			return errors.New("path not set. aborting")
		}
		err := initPasswordStoreEnvironment()
		if err != nil {
			return err
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {

		gkb, err := api.NewGopassKubesealBridge(cmd.Context(), config, kubeseal.SealedSecretsConfig{
			ControllerNamespace: controllerNamespace,
			ControllerName:      controllerName,
			ControllerPort:      controllerPort,
		})

		if err != nil {
			return err
		}

		ns, _, _ := namespaceFromClientConfig()

		secret := api.SecretBuilder(applySecretPath, applySecretTargetNamespaces, applySecretSecretName, ns)
		err = gkb.ApplySecrets(secret, dryRun, applyDisableOutput, ns)
		if err != nil {
			return err
		}
		return nil
	},
}

func init() {
	applySecretCmd.Flags().StringVarP(&applySecretPath, "path", "p", "", "Path in the password store. Required")
	applySecretCmd.Flags().StringSliceVarP(&applySecretTargetNamespaces, "target-namespace", "t", []string{}, "Namespace to deploy the secret to. Defaults to the namespace your kubeconfig is pointing to")
	applySecretCmd.Flags().StringVarP(&applySecretSecretName, "secret-name", "s", "", "Name of the sealed secret to create. Defaults to normalized given path (replaces slashes with dashes)")
	applyCmd.AddCommand(applySecretCmd)
}
